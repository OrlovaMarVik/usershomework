﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using UsersWebAPI.DataAccess;
using YamlDotNet.Core.Tokens;

namespace UsersWebAPI.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IRepository<User> _userRepository;

        public UsersController(ILogger<UsersController> logger, IRepository<User> userRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data = new { status = "Ok" };
            return Ok(data);
        }

        private void WaitInterval(int from, int to)
        {
            Random rnd = new Random();

            int value = rnd.Next(from, to);

            Thread.Sleep(value);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            WaitInterval(100, 200);

            var user = await _userRepository.GetByIdAsync(id);

            if (user == null)
                return NotFound();

            return user;
        }

        [HttpPost]
        public async Task<ActionResult<User>> Post(User user)
        {
            WaitInterval(0, 100);
            try
            {
                await _userRepository.AddAsync(user);

                _logger.LogInformation("user.Id=" + user.Id.ToString());
                var userResult = await _userRepository.GetByIdAsync(user.Id);

                if (userResult == null)
                    return NotFound();

                return Ok();
            }

            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Put(int id, User user)
        {
            var userResult = await _userRepository.GetByIdAsync(id);

            if (userResult == null)
                return NotFound();

            await _userRepository.UpdateAsync(user);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> Delete(int id)
        {
            var userResult = await _userRepository.GetByIdAsync(id);

            if (userResult == null)
                return NotFound();

            await _userRepository.DeleteAsync(userResult);

            return Ok();
        }
    }
}
