﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using UsersWebAPI.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace UsersWebAPI.MigrationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Applying migrations");
            var webHost = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<ConsoleStartup>()
                .Build();
            using (var context = (DataContext)webHost.Services.GetService(typeof(DataContext)))
            {
                context.Database.Migrate();
            }
            Console.WriteLine("Done");
        }
    }
}
