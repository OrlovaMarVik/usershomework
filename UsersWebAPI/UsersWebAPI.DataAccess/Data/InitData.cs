﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UsersWebAPI.DataAccess.Data
{
    public static class InitData
    {
        public static IEnumerable<User> Users => new List<User>()
        {
            new User()
            {
                Id = 1,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Иванов",
                Login = "ivanov",
                Phone = "+7 915 777-77-77"
            },
            new User()
            {
                Id = 2,
                Email = "andreev@somemail.ru",
                FirstName = "Андрей",
                LastName = "Андреев",
                Login = "andreev",
                Phone = "+7 915 888-77-77"
            }
        };
    }
}
