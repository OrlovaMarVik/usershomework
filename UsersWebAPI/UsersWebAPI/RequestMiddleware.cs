﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Prometheus;
using System.Threading.Tasks;
using System;
using System.Diagnostics;

namespace UsersWebAPI.WebHost
{
    public class RequestMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public RequestMiddleware(
            RequestDelegate next
            , ILoggerFactory loggerFactory
            )
        {
            this._next = next;
            this._logger = loggerFactory.CreateLogger<RequestMiddleware>();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var path = httpContext.Request.Path.Value;
            var method = httpContext.Request.Method;

            var counter = Metrics.CreateCounter("prometheus_request_total", "HTTP Requests Total", new CounterConfiguration
            {
                LabelNames = new[] { "path", "method", "status" }
            });

            var statusCode = 200;
            var sw = Stopwatch.StartNew();

            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception)
            {
                statusCode = 500;
                counter.Labels(path, method, statusCode.ToString()).Inc();

                throw;
            }
            sw.Stop();

            if (path != "/metrics" && !path.Contains( "swagger"))
            {
                statusCode = httpContext.Response.StatusCode;
                counter.Labels(path, method, statusCode.ToString()).Inc();

                var config = new HistogramConfiguration { /*Buckets = new[] { 0.5, 0.95, 0.99 },*/ LabelNames = new[] {"method", "path"} };
                var histogram =
                Metrics
                    .CreateHistogram(
                        "api_response_time_seconds",
                        "API Response Time in seconds",
                        config
                        );

                histogram
                    .WithLabels(httpContext.Request.Method, httpContext.Request.Path)
                    .Observe(sw.Elapsed.TotalSeconds);
            }
        }
    }

    public static class RequestMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestMiddleware>();
        }
    }
}
