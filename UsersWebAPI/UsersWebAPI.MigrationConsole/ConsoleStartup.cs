﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using UsersWebAPI.DataAccess;

namespace UsersWebAPI.MigrationConsole
{
    public class ConsoleStartup
    {
        public ConsoleStartup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            var envServer = Environment.GetEnvironmentVariable("POSTGRES_SERVER");
            var envPort = Environment.GetEnvironmentVariable("POSTGRES_PORT");
            var envDatabase = Environment.GetEnvironmentVariable("POSTGRES_DATABASE");
            var envUser = Environment.GetEnvironmentVariable("POSTGRES_USER");
            var envPassword = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");

            services.AddDbContext<DataContext>(x =>
            {
                x.UseNpgsql($"Server={envServer};Port={envPort};Database={envDatabase};Username={envUser};Password={envPassword}");
            });
        }
        public void Configure(IApplicationBuilder app)
        {

        }
    }
}
