﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UsersWebAPI.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
