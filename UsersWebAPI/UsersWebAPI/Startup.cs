using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UsersWebAPI.DataAccess;
using UsersWebAPI.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Prometheus;

namespace UsersWebAPI.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerDocument();
            services.AddControllers();

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            var envServer = Environment.GetEnvironmentVariable("POSTGRES_SERVER");
            var envPort = Environment.GetEnvironmentVariable("POSTGRES_PORT");
            var envDatabase = Environment.GetEnvironmentVariable("POSTGRES_DATABASE");
            var envUser = Environment.GetEnvironmentVariable("POSTGRES_USER");
            var envPassword = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");

            services.AddDbContext<DataContext>(x =>
            {
                x.UseNpgsql($"Server={envServer};Port={envPort};Database={envDatabase};Username={envUser};Password={envPassword}");
                //x.UseNpgsql($"Server=postgres;Port=5432;Database=Users;Username=postgres;Password=password");
            });

            services.AddScoped<IDbInitializer, EfDbInitializer>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMetricServer();
            app.UseRequestMiddleware();

            app.UseOpenApi();
            app.UseSwaggerUi();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //dbInitializer.InitializeDb();
        }
    }
}
