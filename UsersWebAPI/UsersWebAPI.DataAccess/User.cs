namespace UsersWebAPI.DataAccess
{
    public class User: BaseEntity
    {
        public string Login  { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}
