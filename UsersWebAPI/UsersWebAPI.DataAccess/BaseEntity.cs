﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UsersWebAPI.DataAccess
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
